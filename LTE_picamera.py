#! /usr/bin/env python
# LTE_picamera.py

# SPDX-FileCopyrightText: 2021 Anne Barela for Adafruit Industries
#
# SPDX-License-Identifier: MIT

from picamera import PiCamera
import time
from fractions import Fraction
import datetime
import numpy as np
#from matplotlib import pyplot as plt
import sys
from PIL import Image
import os

type = sys.argv[1]
sample_id= sys.argv[2]
pose = float(sys.argv[3])
directory="/home/bs/Documents/SMART3D/Camera/data/"


cur_time = datetime.datetime.now()
timestamp=cur_time.strftime("%Y%m%d%H%M")
daystamp=cur_time.strftime("%Y%m%d")
stub = directory+type+'_'+sample_id+'_'+timestamp
outfile = "%s.pgm" % (stub)
reportfile = directory+"experiment_"+daystamp+".txt"

output = np.empty((432,768,3), dtype=np.uint8)
camera = PiCamera(framerate=Fraction(1,6))

# You can change these as needed. Six seconds (6 000 000)
# is the max for shutter speed and 800 is the max for ISO.
#pose= 1  #time exposure in s
camera.shutter_speed = int(pose*1000000)
camera.iso = 800

time.sleep(2)
camera.exposure_mode = 'off'
camera.resolution=(768,432)
camera.capture(output,'rgb')
camera.close()
report_str=timestamp + ", " + type + ", " + sample_id + ", " + sys.argv[3] + ", " + outfile+ "\n"
print(report_str)


im=Image.fromarray(output)
im.show()
print("keep record ? press k for keeping or any key to quit")
for line in sys.stdin:
	for var in line.split():
		if var == "k":
			im.save(outfile)
			answ=os.path.exists(reportfile)
			if (answ):
				w="a"
			else:
				w="w"
			f = open(reportfile,w)
			f.write(report_str)
			print("saved")
			f.close()
			sys.exit()
		else:
			print("Quitting without saving")
			sys.exit()
