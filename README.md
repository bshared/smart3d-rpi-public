# SMART3D
* Installation
Pour installer ou mettre à jour l'ensemble des programmes sur votre Raspberry et générer le structure de dossier nécessaire, merci de suivre ces étapes:

1. Vérifier que le Raspberry est connecté à internet
2. Cloner ce dépôt en tapant dans le terminal la commande suivante :
```shell
git clone https://gitlab.com/bshared/smart3d-rpi-public.git
```

3. Exécuter le fichier d'installation en tapant dans le terminal les lignes suivantes :
```shell
cd smart3d-rpi-public
chmod +x install.sh
./install.sh
```
* Utilisation du programme de capture
Pour utiliser le programme de capture il faut ouvrir un terminal et taper la commande :
```shell
smart3d_capture type_echantillon identifiant temps_pose
```
Attention ! le nom du fichier est composé de ces différents arguments. Aussi il faudra éviter d'utiliser des chiffre en premier caractère des arguments et tous les caractères spéciaux en particulier (/ et ,). Pour les décimales, il faut utiliser le caractère "." .

