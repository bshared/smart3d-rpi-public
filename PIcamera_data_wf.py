import csv
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from numpy import asarray
from tabulate import tabulate

def result_path(exp_file):
    """extract path name for results, create it if not exists and give it"""
    sname = exp_file.split('/')
    name=sname[len(sname)-1].split('.')
    print(name[0])
    res_path = ("../results/" + name[0]+"/")
    if not os.path.exists(res_path):
        os.makedirs(res_path)
    return res_path


def get_image_array(image_file):
    print("opening", image_file, "... ")
    left = 347
    top = 237
    right = left + 53
    bottom = top + 5
    image = Image.open(image_file).crop((left, top, right, bottom))
    # image.show()
    result = asarray(image)
    print("array shape :", result.shape)
    return result


def set_axis_style(ax, labels):
    ax.get_yaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_yticks(np.arange(1, len(labels) + 1))
    ax.set_yticklabels(labels)
    ax.set_ylim(0.25, len(labels) + 0.75)
    ax.set_ylabel('Sample name')


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                '%d' % int(height),
                ha='center', va='bottom')


red = []
blue = []
green = []
combined = []
labels = []
means_red = []
medians_red = []
means_green = []
medians_green = []
means_blue = []
medians_blue = []
means_combined = []
medians_combined = []
stds_red = []
stds_green = []
stds_combined = []
hop = sys.argv[0]
print(hop)
experiment_file = sys.argv[1]
meta = []
poses = []
image_file = "../data/D1_rhodB_202211181514.pgm"
with open(experiment_file, 'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        filepath = row[4].replace(" ", "").split('/')
        path = (os.path.join(os.path.dirname(__file__), filepath[0], filepath[1], filepath[2]))
        print(row[3])
        pose = float(row[3].replace(" ", ""))
        print(pose)
        poses.append(pose)
        label = row[1].replace(" ", "")
        imarr = get_image_array(path)
        r, g, b = imarr[:, :, 0], imarr[:, :, 1], imarr[:, :, 2]
        labels.append(label)
        red.append((r.flatten()) / pose)
        green.append((g.flatten()) / pose)
        blue.append((b.flatten()) / pose)
        means_red.append(np.mean(r.flatten() / pose))
        means_green.append(np.mean(g.flatten() / pose))
        medians_red.append(np.median(r.flatten() / pose))
        medians_green.append(np.median(g.flatten() / pose))
        medians_blue.append(np.median(b.flatten() / pose))
        means_blue.append(np.mean(b.flatten() / pose))
        combined.append((r.flatten() + g.flatten() - b.flatten()) / pose)
        means_combined.append(np.mean((r.flatten() + g.flatten() - b.flatten()) / pose))
        medians_combined.append(np.median((r.flatten() + g.flatten() - b.flatten()) / pose))
        stds_red.append(np.std(r.flatten())/pose)
        stds_green.append(np.std(g.flatten()) / pose)
print("done...")
print("red channel", len(red))
print("green channel ", len(green))
print("blue channel ", len(blue))
print("means red\n", means_red)
print("means green\n", means_green)
print("means blue\n", means_blue)
print("median red\n", medians_red)
print("median green\n", medians_green)
print("median blue\n", medians_blue)

print(result_path(experiment_file))
fig, ax = plt.subplots(figsize=(5, 9))
# ax.violinplot(red, positions=[1,2,3,4,5,6,7,8,9], vert=False, showmeans=True, showmedians=True)
part_blue = ax.violinplot(blue, vert=False, showmeans=True, showmedians=True)
part_red = ax.violinplot(red, vert=False, showmeans=True, showmedians=True)
part_green = ax.violinplot(green, vert=False, showmeans=True, showmedians=True)
part_combine = ax.violinplot(combined, vert=False, showmeans=True, showmedians=True)
# part_blue = ax.violinplot(blue, vert=False, showmeans=True, showmedians=True)
# part_red = ax.violinplot(red, vert=False, showmeans=True, showmedians=True)
# part_green = ax.violinplot(green, vert=False, showmeans=True, showmedians=True)
for pc in part_red['bodies']:
    pc.set_facecolor('red')
    pc.set_edgecolor('black')
    pc.set_alpha(.5)
for pc in part_green['bodies']:
    pc.set_facecolor('green')
    pc.set_edgecolor('black')
    pc.set_alpha(.5)
for pc in part_blue['bodies']:
    pc.set_facecolor('blue')
    pc.set_edgecolor('black')
    pc.set_alpha(.5)
for pc in part_combine['bodies']:
    pc.set_facecolor('gray')
    pc.set_edgecolor('black')
    pc.set_alpha(.5)
set_axis_style(ax, labels)

plt.show()
plt.savefig(result_path(experiment_file)+"violins.svg")

# diagrams on means and medians
N = len(means_red)
ind = np.arange(N)
width = 0.9

fig, ax = plt.subplots()
rects1 = ax.bar(ind, means_red, width, color='r')
ax.set_ylabel('Normalised intensity')
ax.set_title('red means by samples')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(labels)
autolabel(rects1)
plt.savefig(result_path(experiment_file)+"red_means.svg")
fig, ax = plt.subplots()
rects1 = ax.bar(ind, medians_red, width, color='m')
ax.set_ylabel('Normalised intensity')
ax.set_title('red medians by samples')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(labels)
autolabel(rects1)
plt.savefig(result_path(experiment_file)+"red_medians.svg")

fig, ax = plt.subplots()
rects1 = ax.bar(ind, means_combined, width, color='g')
ax.set_ylabel('Normalised intensity')
ax.set_title('corrected means by samples')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(labels)
autolabel(rects1)
plt.savefig(result_path(experiment_file)+"corrected_means.svg")

fig, ax = plt.subplots()
rects1 = ax.bar(ind, medians_combined, width, color='k')
ax.set_ylabel('Normalised intensity')
ax.set_title('corrected median by samples')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(labels)
autolabel(rects1)
plt.savefig(result_path(experiment_file)+"corrected_medians.svg")
res_tab= []
for i,j,k,l,m,n,o in zip(labels, means_red,medians_red,stds_red,means_green,medians_green,stds_green):
    res_tab.append([i,j,k,l,m,n,o])
print(tabulate(res_tab,headers=['sample', 'red mean', 'red median', 'red stddev','green mean','green median','green stddev'], tablefmt='orgtbl'))
